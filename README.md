Step to install
 -- composer install
 -- php artisan key:generate
create .env file and set db details

commands:
php artisan migrate
php artisan module:seed --class=AdminDatabaseSeeder Admin
php artisan storage:link

Admin credentials :
superadmin@gmail.com/password
admin@gmail.com / password

For real time notifiaction update
php artisan websockets:serve

Links
Admin login :
http://127.0.0.1:8000/admin/login

Customer Login :
http://127.0.0.1:8000/customers/login
http://127.0.0.1:8000/customers/register

Book Vehicle : http://127.0.0.1:8000/customer/book-vehicle