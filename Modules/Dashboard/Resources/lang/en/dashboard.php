<?php
return [
    'titles' => [
        'head_title' => 'Dashboard',
        'dashboard' =>  'Dashboard',
    ],
    'labels' => [
        'index' => 'Dashboard Page'
    ],
    'messages' => [
        'welcome_text' => 'Welcome on your backend!'
    ]
];