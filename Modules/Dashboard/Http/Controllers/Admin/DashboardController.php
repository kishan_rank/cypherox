<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use App\Notifications\LeaveUpdateNotification;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Admin;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('dashboard::admin.index');
    }
}
//$user->notify(new App\Notifications\LeaveUpdateNotification('Hello World'));