@if (Session::has('success'))
    <div class="callout callout-success">
        <p class="text-success">{{ Session::get('success') }}</p>
    </div>
@endif

@if (Session::has('error'))
    <div class="callout callout-danger">
        <p class="text-danger">{{ Session::get('error') }}</p>
    </div>
@endif

@if (Session::has('warning'))
    <div class="alert alert-warning alert-dismissible">
        <p class="text-warning">{{ Session::get('warning') }}</p>
    </div>
@endif

@if (Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <p class="text-info">{{ Session::get('info') }}</p>
    </div>
@endif
<style type="text/css">
    .callout .callout-success {
        border-left-color: #28a745;
    }

    .callout .callout-danger {
        border-left-color: red;
    }

</style>
