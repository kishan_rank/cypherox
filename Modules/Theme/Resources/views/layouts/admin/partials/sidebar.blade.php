<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-info elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="https://picsum.photos/70" alt="Admin Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-bold">Admin Panel</span>
    </a>
    @if ($sidebarMenu)
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach ($sidebarMenu as $group => $items)
                @if (strpos($group, 'core::core.menu.single') !== false)
                @foreach ($items as $menuItem)
                <li class="nav-item">
                    <a href="{{ route($menuItem['route']) }}" class="nav-link {{ (isset($menuItem['active_actions']) && in_array($currentRoute, $menuItem['active_actions'])) ? 'active' : '' }}">
                        <i class="{{ (isset($menuItem['icon']) && $menuItem['icon']) ? $menuItem['icon'] : 'far fa-circle nav-icon' }}"></i>
                        <p>{{ trans($menuItem['title']) }}</p>
                        @if(isset($menuItem['create']) && !empty($menuItem['create']))
                        <object class="right"><a href="{{route($menuItem['create'])}}" class="font-weight-bold"><i class="fa fa-plus fa-sm"></i></a></object>
                        @endif
                    </a>
                </li>
                @endforeach
                @else
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link nav-group-link">
                        <i class="nav-icon {{ (isset($menuItem['group_icon']) && $menuItem['group_icon']) ? $menuItem['group_icon'] : 'fas fa-layer-group' }}"></i>
                        <p>
                            {{ trans($group) }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @foreach ($items as $menuItem)
                        <li class="nav-item">
                            <a href="{{ route($menuItem['route']) }}" class="nav-link {{ (isset($menuItem['active_actions']) && in_array($currentRoute, $menuItem['active_actions'])) ? 'active' : '' }}">
                                <i class="{{ (isset($menuItem['icon']) && $menuItem['icon']) ? $menuItem['icon'] : 'far fa-circle nav-icon' }}"></i>
                                <p>{{ trans($menuItem['title']) }}</p>
                                @if(isset($menuItem['create']) && !empty($menuItem['create']))
                                <object class="right"><a href="{{route($menuItem['create'])}}" class="font-weight-bold"><b><i class="fas fa-plus fa-sm"></i></b></a></object>
                                @endif
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endif
                @endforeach
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
    @endif
</aside>
<script type="text/javascript">
</script>