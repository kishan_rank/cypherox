<?php
return [
    'titles' => [
        'head_title'=>'Theme',
        'title'=>'Title',
        'content'=>'Content',
        'access_key'=>'Access_key',
        'is_enabled'=>'Is Enabled',
        'language'=>'Language',
        'create_block'=>'Create Block',
        'block_info'=>'Block Information',
        'theme'=>'Layout Themes',
        'theme_settings' => 'Theme Settings'
    ],
    'buttons'=>
    [
        'save' => 'Save',
        'reset_settings' => 'Reset Settings',
        'reset' => 'Reset',
        'cancel' => 'Cancel'
    ],
    'labels' => [
        'index'=>'Theme Page',
        'settheme'=>'Set Theme',
        'resettheme'=>'Reset Theme',
        'title' => 'Title',
        'content' => 'Content',
        'access_key'=>'Access key',
        'status'=>'Status',
        'type'=>'Type',
        'logo'=>'Admin Logo',
        'sidebarCollapse'=>'Sidebar Collapse',
        'legacyStyle'=>'Legacy Style',
        'sidebarNav'=>'Sidebar Nav',
        'bodySmallText'=>'Body Small Text',
        'defualtlogo'=>'Defualt logo'
    ],
    'options'=> [
        'access'=> [
            'web'=>'Web',
            'mobile'=>'Mobile',
        ]
    ],
    'messages' => [
        'confirm'=>'Sure You Want To Reset Theme,All Theme Change Will Remove !!',
        'updated_success' => 'Themes updated successfully.',
        'reset_success' => 'Themes Reset successfully',
    ],
    'settings' => [
        'design' => 'Design',
        'extra_css' => 'Extra Css',
        'extra_js' => 'Extra Js'
    ],
    'comment' => [
        'extra_css' => 'Add extra css here to utilize it in admin site.',
        'extra_js' => 'Add extra js here to utilize it in admin site.'
    ],
];