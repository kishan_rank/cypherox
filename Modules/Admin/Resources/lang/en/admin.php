<?php

return [
    'titles' => [
        'manage_admin' => 'Manage Admin Users',
        'admin_profile' => 'Admin Profile',
        'create_admin' => 'Create Admin User',
        'edit_admin' => 'Edit Admin User'
    ],
    'menu' => [
        'admin' => 'Admin User',
    ],
    'buttons' => [
        'cancel' => 'Cancel',
        'create' => 'Create New',
        'save' => 'Save',
        'update' => 'Update',
        'delete' => 'Delete',
        'back' => 'Back'
    ],
    'grid' => [
        'header' => [
            'name'       => 'User Name',
            'no'         => 'No.',
            'email'      => 'E-mail',
            'status'     => 'Status',
            'confirmed'  => 'Confirmed',
            'role'       => 'Role',
            'created_at' => 'Created At',
            'action'     => 'Actions',
            'type'       => 'User Type'
        ],
    ],
    'form' => [
        'label' => [
            'name' => 'Name : ',
            'email' => 'E-mail Address : ',
            'password' => 'Password : ',
            'password_confirmation' => 'Password Confirmation : ',
            'associated-role' => 'Associated Role : ',
            'all' => 'All',
            'custom' => 'Custom',
        ],
        'input' => [],
        'placeholder' => [
            'name' => 'Enter Admin Name',
            'email' => 'Enter E-mail Address',
            'password' => 'Enter Password',
            'password_confirmation' => 'Enter Confirmed Password'
        ],
    ],
];
