<?php

use Illuminate\Support\Facades\Route;

Route::prefix('adminuser')->group(function () {
    Route::get('/users', [
        'as' => 'admin.adminuser.index',
        'uses' => 'AdminController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.adminuser.get',
        'uses' => 'AdminController@get',
        'middleware' => 'auth:admin'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'admin.adminuser.destroy',
        'uses' => 'AdminController@destroy',
        'middleware' => 'auth:admin'
    ]);
});

Route::get('/account/profile', [
    'as' => 'admin.adminuser.profile',
    'uses' => 'AdminController@profile',
    'middleware' => 'auth:admin'
]);
Route::post('/update/password', [
    'as' => 'admin.adminuser.updatepassword',
    'uses' => 'AdminController@updatePassword',
    'middleware' => 'auth:admin'
]);
Route::put('/update/profile', [
    'as' => 'admin.adminuser.profile.update',
    'uses' => 'AdminController@updateProfile',
    'middleware' => 'auth:admin'
]);
