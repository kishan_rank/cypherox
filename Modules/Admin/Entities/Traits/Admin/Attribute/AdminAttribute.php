<?php

namespace Modules\Admin\Entities\Traits\Admin\Attribute;

use Modules\Admin\Entities\Admin;

trait AdminAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        if ($this->permission != 2) {
            $data .= $this->getDeleteButtonAttribute('admin.adminuser.destroy');
        }
        $data .= '</div>';
        return $data;
    }

    public function getConfirmedForGridAttribute()
    {
        if ($this->confirmed) {
            return '<label class="badge badge-success">' . Admin::CONFIRMED_YES_TEXT . '</label>';
        }
        return '<label class="badge badge-danger">' . Admin::CONFIRMED_NO_TEXT . '</label>';
    }

    public function getPermissionForGridAttribute()
    {
        if ($this->permission == 2) {
            return '<label class="badge badge-info">Super Admin</label>';
        }
        return '<label class="badge badge-info">Admin</label>';
    }
}
