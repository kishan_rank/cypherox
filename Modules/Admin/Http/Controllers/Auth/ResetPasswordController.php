<?php

namespace Modules\Admin\Http\Controllers\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Password;
use Modules\Admin\Entities\Admin;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = Admin::REDIRECT_TO;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null){
        return view('admin::auth.passwords.reset',[
            'passwordUpdateRoute' => 'admin.password.update',
            'token' => $token,
            'email' => $request->email
        ]);
    }

    protected function broker(){
        return Password::broker('admins');
    }

    protected function guard(){
        return Auth::guard('admin');
    }

    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->setRememberToken(Str::random(60));

        $user->save();
    }

    protected function sendResetResponse(Request $request, $response)
    {
        if ($request->wantsJson()) {
            return new JsonResponse(['message' => trans($response)], 200);
        }

        $notification = array(
            'message' => 'Your account password has been successfully changed.',
            'alert-type' => 'success'
        );

        return redirect(route('admin.login'))
            ->with($notification);
    }
}
