<?php

namespace Modules\Core\Composers;

use Illuminate\Http\Request;
use Modules\Core\Foundations\Menu;
use Illuminate\Contracts\View\View;

class MenuComposer
{
    /**
     * @var Menu
     */
    protected $menu;

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request, Menu $menu)
    {
        $this->menu = $menu;
    }

    public function compose(View $view)
    {
        $view->with('sidebarMenu', $this->menu->getItems());
        // $view->with('authUser', $this->menu->getAuthUser());
        $view->with('currentRoute', \Request::route()->getName());
    }
}

