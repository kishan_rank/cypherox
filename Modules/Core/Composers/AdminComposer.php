<?php

namespace Modules\Core\Composers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class AdminComposer
{
    public function compose(View $view)
    {
        $view->with('admin', Auth::guard('admin')->user()->attributesToArray());
    }
}