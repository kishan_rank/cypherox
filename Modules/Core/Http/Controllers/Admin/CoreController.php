<?php

namespace Modules\Core\Http\Controllers\Admin;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Routing\Controller;

class CoreController extends Controller
{
    public function successMessageResponse(array $data, $code = 200)
    {
        return response()->json(['type' => 'success', 'content' => $data, 'code' => $code], $code);
    }

    public function errorMessageResponse(array $data, $code = 400)
    {
        return response()->json(['type' => 'error', 'content' => $data, 'code' => $code], $code);
    }

    public function successRedirect($route, $message = null)
    {
        return redirect()->route($route)->with('success', $message);
    }

    public function redirectWithParameter($route, $parameter, $message = null)
    {
        return redirect()->route($route, $parameter)->with('success', $message);
    }

    public function errorRedirect($route, $message = null)
    {
        return redirect()->route($route)->with('error', $message);
    }

    public function backWithError($message)
    {
        return back()->with('error', $message)->withInput();
    }

    public function backWithSuccess($message)
    {
        return back()->with('success', $message);
    }

    public function saveImage($imageData)
    {
        if (!isset($imageData['file'])) {
            throw new Exception('Invalid Image Data.');
        }
        $image = $imageData['file'];
        $filenameWithExt = $image->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $filename = str_replace(" ", "_", strtolower($filename));
        $extension = $image->getClientOriginalExtension();
        $fileNameToStore = $filename . '_' . time() . '.' . $extension;

        $img = Image::make($image->getRealPath())->resize(300, 300)->encode();
        $imageSavePath = $imageData['storage_path'] . $fileNameToStore;
        Storage::disk($imageData['disk'])->put($imageSavePath, $img);
        return $fileNameToStore;
    }
}
