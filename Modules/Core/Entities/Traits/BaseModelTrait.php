<?php

namespace Modules\Core\Entities\Traits;

trait BaseModelTrait
{

    public function getEditButtonAttribute($route, $isDisable = false, $disableMessage = null)
    {
        if ($isDisable) {
            $class = 'disable-edit-message';
        } else {
            $class = '';
        }
        return '<a href="' . route($route, $this) . '" data-message="' . $disableMessage . '" title="Edit" class="btn btn-xs btn-flat btn-info mr-2 ' . $class . '"><i class="fa fa-edit"></i></a>';
    }

    public function getEditButtonByRouteAttribute($route)
    {
        return '<a href="' . $route . '" title="Edit" class="btn btn-xs btn-flat btn-info mr-2"><i class="fa fa-edit"></i></a>';
    }

    public function getViewButtonByRouteAttribute($route)
    {
        return '<a href="' . $route . '" title="Edit" class="btn btn-xs btn-flat btn-info mr-2"><i class="fa fa-eye"></i></a>';
    }

    public function getDeleteButtonAttribute($route, $isDisable = false, $disableMessage = null)
    {
        if ($isDisable) {
            $class = 'disable-delete-message';
        } else {
            $class = 'delete-entry';
        }
        return '<a href="' . route($route, $this) . '" title="Delete" data-message="' . $disableMessage . '" class="btn btn-xs btn-flat btn-danger mr-2 ' . $class . '" data-method="delete"><i class="fa fa-trash"></i></a>';
    }

    public function getCheckboxInputAttribute($name)
    {
        return '<input type="checkbox" name="' . $name . '" class="' . $name . '_checkbox float-center" value="' . $this->id . '">';
    }

    public function getViewButtonWithPopupAttribute($route)
    {
        return '<button data-url="' . route($route, $this) . '" title="Delete" class="btn btn-xs btn-flat btn-info view-popup" data-id="' . $this->id . '" data-method="view"><i class="fa fa-eye"></i></button>';
    }

    public function getRestoreButtonAttribute($route)
    {
        return '<a href="' . route($route, $this) . '" title="Restore" class="btn btn-xs btn-flat btn-info mr-2"><i class="fa fa-history"></i></a>';
    }

    public function getForceDeleteButtonAttribute($route)
    {
        return '<a href="' . route($route, $this) . '" title="Force Delete" class="btn btn-xs btn-flat btn-danger mr-2 delete-entry" data-method="delete"><i class="fa fa-trash"></i></a>';
    }
}
