<?php
namespace Modules\Core\Foundations\Modules;

use Illuminate\Support\Collection;
use Modules\Core\Foundations\Menu as MenuInterFace;
use Illuminate\Support\Facades\Auth;

final class Menu implements MenuInterFace
{
    /**
     * @var array
     */
    protected $items = [];

    public function __construct()
    {
        $this->items = new Collection();
    }

    /**
     * Add an array of menu items
     * @param string $item
     * @return void
     */
    public function addMenuItems(array $items)
    {
        // print_r(111);die;
        foreach ($items as $item) {
            $this->addMenuItem($item);
        }
    }

    /**
     * Add an array of menu item
     * @param array $item
     * @return void
     */
    public function addMenuItem($item)
    {
        return $this->items->push($item);
    }
    
    /**
     * @return collection
     */
    public function getItems()
    {
        $collection = $this->items->sortBy("order")->groupBy("group");
        return $collection->toArray();
    }

    public function getAuthUser()
    {
        if(Auth::check()) {
            return Auth::user();
        }
        return [];
    }
}