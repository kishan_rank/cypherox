<?php

namespace Modules\Core\Foundations;

interface Menu
{
    /**
     * Add an array of menu items
     * @param array $items
     * @return void
     */
    public function addMenuItems(array $items);

    /**
     * Add an array of menu item
     * @param array $item
     * @return void
     */
    public function addMenuItem($item);

    /**
     * @return collection
     */
    public function getItems();
}
