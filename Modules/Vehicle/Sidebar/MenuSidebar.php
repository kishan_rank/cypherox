<?php

namespace Modules\Vehicle\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.manage_vehicle",
            "title" => "vehicle::vehicle.titles.manage_vehicle",
            "route" => "admin.vehicles.index",
            "icon" => "fas fa-circle nav-icon",
            "active_actions" => [
                "admin.vehicles.index"
            ],
            "order" => 10,
        ];

        $menuItems2 = [
            "group" => "core::core.menu.single.manage_vehicle_booking",
            "title" => "vehicle::vehicle.titles.manage_vehicle_booking",
            "route" => "admin.vehicles-booking.index",
            "icon" => "fas fa-circle nav-icon",
            "active_actions" => [
                "admin.vehicles-booking.index"
            ],
            "order" => 12,
        ];

        $this->_menu->addMenuItem($menuItems);
        $this->_menu->addMenuItem($menuItems2);
    }
}
