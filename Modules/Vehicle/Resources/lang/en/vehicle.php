<?php

return [
    'titles' => [
        'manage_vehicle' => 'Manage Vehicle',
        'manage_vehicle_booking' => 'Manage Booking'
    ],
    'grid' => [
        'header' => [
            'price' => 'Price',
            'name' => 'Name',
            'created_at' => 'Created At',
            'no' => 'No.',
            'name' => 'Name',
            'email' => 'Email',
            'vehicle_name' => 'Vehicle Name',
            'start_date' => 'Start Date',
            'booking_for' => 'Booking For',
            'zip' => 'Zip Code',
            'city' => 'City',
            'state' => 'State'
        ]
    ],
    'form' => [
        'label' => [
            'name' => 'Name',
            'price' => 'Price'
        ],
        'placeholder' => [
            'name' => 'Enter Name',
            'price' => 'Enter Price'
        ],
    ],
    'buttons' => [
        'save' => 'Save',
        'cancel' => 'Cancel'
    ]
];
