@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('vehicle::vehicle.titles.manage_vehicle') }}
@endsection

@section('extra-css')
    @include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('vehicle::vehicle.titles.manage_vehicle') }}</h1>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-info" href="{{ route('admin.vehicles.create') }}">Create New</a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-body">
                        <div class="table-responsive data-table-wrapper">
                            <table id="vehicles-table" class="table table-condensed table-hover table-bordered"
                                width="100%">
                                <thead class="transparent-bg">
                                    <tr>
                                        <th>{{ trans('vehicle::vehicle.grid.header.no') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.name') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.price') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.created_at') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    @include('theme::layouts.admin.modals.confirm')
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('after-js')
    @include('theme::asset.admin.js.datatable')
    <script>
        $(function() {
            var dataTable = $('#vehicles-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.vehicles.get') }}',
                    type: 'post'
                },
                lengthMenu: [20, 30, 50, 100, 200],
                pageLength: 20,
                columns: [{
                        data: 'id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'price'
                    },
                    {
                        data: 'created_at'
                    },
                ],
                searchDelay: 500,
            });
        });
    </script>
@stop
