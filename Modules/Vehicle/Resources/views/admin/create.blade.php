@extends('theme::layouts.admin.master')

@section('title')
    {{ 'Add Vehicle' }}
@endsection

@section('extra-css')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ 'Add Vehicle' }}</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    {!! getBackButton('admin.vehicles.index') !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.vehicles.store') }}" class="form-horizontal setting-form" id="validate"
                    enctype='multipart/form-data' method="POST">
                    @csrf
                    <div class="card card-info card-outline">
                        <div class="card-body">
                            <div class="form-group row">
                                {{ Form::label('name', trans('vehicle::vehicle.form.label.name'), ['class' => 'col-md-2 control-label label-right required']) }}

                                <div class="col-md-10">
                                    {{ Form::text('name', null, ['class' => 'form-control box-size','id' => 'name','autocomplete' => 'off','placeholder' => trans('vehicle::vehicle.form.placeholder.name'),'required' => 'required']) }}
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('price', trans('vehicle::vehicle.form.label.price'), ['class' => 'col-md-2 control-label label-right required']) }}

                                <div class="col-md-10">
                                    {{ Form::number('price', null, ['class' => 'form-control box-size','id' => 'price','autocomplete' => 'off','placeholder' => trans('vehicle::vehicle.form.placeholder.price'),'required' => 'required']) }}
                                    @error('price')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="edit-form-btn">
                                {{ link_to_route('admin.vehicles.index',trans('vehicle::vehicle.buttons.cancel'),[],['class' => 'btn btn-danger btn-md']) }}
                                {{ Form::submit(trans('vehicle::vehicle.buttons.save'), ['class' => 'btn btn-primary btn-md']) }}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('after-js')
@stop
