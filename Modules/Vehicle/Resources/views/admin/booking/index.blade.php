@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('vehicle::vehicle.titles.manage_vehicle_booking') }}
@endsection

@section('extra-css')
    @include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('vehicle::vehicle.titles.manage_vehicle_booking') }}</h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-body">
                        <div class="table-responsive data-table-wrapper">
                            <table id="vehicles-booking" class="table table-condensed table-hover table-bordered"
                                width="100%">
                                <thead class="transparent-bg">
                                    <tr>
                                        <th>{{ trans('vehicle::vehicle.grid.header.no') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.name') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.email') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.vehicle_name') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.start_date') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.booking_for') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.zip') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.city') }}</th>
                                        <th>{{ trans('vehicle::vehicle.grid.header.state') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    @include('theme::layouts.admin.modals.confirm')
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('after-js')
    @include('theme::asset.admin.js.datatable')
    <script>
        $(function() {
            var dataTable = $('#vehicles-booking').DataTable({
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.vehicles-booking.get') }}',
                    type: 'post'
                },
                lengthMenu: [20, 30, 50, 100, 200],
                pageLength: 20,
                columns: [{
                        data: 'id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'vehicle_name'
                    },
                    {
                        data: 'start_date'
                    },
                    {
                        data: 'book_for'
                    },
                    {
                        data: 'zipcode'
                    },
                    {
                        data: 'city'
                    },
                    {
                        data: 'state'
                    },
                ],
                searchDelay: 500,
            });
        });
    </script>
@stop
