<?php

namespace Modules\Vehicle\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Vehicle\Entities\Vehicle;
use Modules\Vehicle\Http\Requests\StoreVehicleRequest;
use Yajra\DataTables\Facades\DataTables;

class VehicleController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('vehicle::admin.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = Vehicle::latest();
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($admin) {
                        return date('d-m-Y H:i:s', strtotime($admin->created_at));
                    })
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('vehicle::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreVehicleRequest $request)
    {
        try {
            $vehicle = Vehicle::create($request->validated());
            if ($vehicle->id) {
                return $this->successRedirect('admin.vehicles.index', 'Vehicle data created successfully.');
            }
            return $this->errorRedirect('admin.vehicles.indexx', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }
}
