<?php

namespace Modules\Vehicle\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Vehicle\Entities\VehicleBooking;
use Yajra\DataTables\Facades\DataTables;

class BookingController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('vehicle::admin.booking.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data =  VehicleBooking::query()
                    ->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_booking.vehicle_id')
                    ->select(
                        'vehicle_booking.*',
                        DB::raw('(vehicles.name) as vehicle_name'),
                    )
                    ->get();
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
