<?php

namespace Modules\Vehicle\Entities;

use Illuminate\Database\Eloquent\Model;

class VehicleBooking extends Model
{
    protected $table = 'vehicle_booking';
    protected $fillable = ['vehicle_id', 'customer_id', 'start_date', 'book_for', 'shift', 'name', 'email', 'birth_date', 'phone_number', 'address', 'zipcode', 'city', 'state'];

    public function isVehicleAlreadyBooked($validatedData)
    {
        $instance = self::getQuery()
            ->where([
                ['start_date', '=', $validatedData['start_date']],
                ['vehicle_id', '=', $validatedData['vehicle_id']],
            ]);

        if ($validatedData['book_for'] == 'full') {
            $instance->where('book_for', '=', 'full')->orWhere('shift', '=', 'morning')->orWhere('shift', '=', 'evening');
        } else {
            $instance->where('shift', '=', $validatedData['shift']);
        }
        return $instance->first();
    }
}
