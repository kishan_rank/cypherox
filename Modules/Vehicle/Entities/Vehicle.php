<?php

namespace Modules\Vehicle\Entities;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = ['name', 'price'];
}
