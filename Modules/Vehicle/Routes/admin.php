<?php


use Illuminate\Support\Facades\Route;

Route::prefix('vehicles')->group(function () {
    Route::get('/', [
        'as' => 'admin.vehicles.index',
        'uses' => 'VehicleController@index',
        'middleware' => ['auth:admin']
    ]);

    Route::post('/get', [
        'as' => 'admin.vehicles.get',
        'uses' => 'VehicleController@get',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/create', [
        'as' => 'admin.vehicles.create',
        'uses' => 'VehicleController@create',
        'middleware' => ['auth:admin']
    ]);

    Route::post('/store', [
        'as' => 'admin.vehicles.store',
        'uses' => 'VehicleController@store',
        'middleware' => ['auth:admin']
    ]);

    Route::get('/bookings', [
        'as' => 'admin.vehicles-booking.index',
        'uses' => 'BookingController@index',
        'middleware' => ['auth:admin', 'check-permission:superadmin']
    ]);

    Route::post('/get-booking', [
        'as' => 'admin.vehicles-booking.get',
        'uses' => 'BookingController@get',
        'middleware' => ['auth:admin', 'check-permission:superadmin']
    ]);
});
