<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Core\Entities\Traits\BaseModelTrait;

class Customer extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use BaseModelTrait;

    protected $table = 'customers';

    const REDIRECT_TO = '/';

    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
