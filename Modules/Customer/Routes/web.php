<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::prefix('/customer')->group(function () {
    Route::get('/book-vehicle', 'IndexController@index')->name('customers.index')->middleware('auth:customer');
    Route::post('/book-vehicle', 'IndexController@book')->name('customers.bookvehicle')->middleware('auth:customer');
});
