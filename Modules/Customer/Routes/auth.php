<?php

use Illuminate\Support\Facades\Route;

Route::name('customers.')->prefix('customers')->group(function () {
    //Login Routes
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('postLogin');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    // Register Route
    Route::get('/register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('/register', 'RegisterController@register')->name('postRegister');
});
