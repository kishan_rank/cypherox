<?php

namespace Modules\Customer\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Http\Requests\BookVehicleRequest;
use Modules\Vehicle\Entities\Vehicle;
use Modules\Vehicle\Entities\VehicleBooking;

class IndexController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $vehicles = Vehicle::all();
        return view('customer::customer.index', compact('vehicles'));
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function book(BookVehicleRequest $request)
    {
        try {
            $validatedData = $request->validated();


            $customer = $this->getCurrentCustomer();
            if (!$customer || !$customer->id) {
                throw new Exception('Invalid customer Details');
            }

            $booking = new VehicleBooking();
            $isAlreadyBooked = $booking->isVehicleAlreadyBooked($request->all());

            if ($isAlreadyBooked && $isAlreadyBooked->id) {
                throw new Exception('Vehicle is already booked for the same time period');
            }

            $validatedData['customer_id'] = $customer->id;

            $vehicleBooking = VehicleBooking::create($validatedData);

            if ($vehicleBooking && $vehicleBooking->id) {
                return $this->successRedirect('customers.index', 'Vehicle Booking request added successfully.');
            }
            return $this->errorRedirect('customers.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    public function getCurrentCustomer()
    {
        return auth()->guard('customer')->user();
    }
}
