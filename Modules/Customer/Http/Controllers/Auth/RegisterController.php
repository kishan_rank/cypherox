<?php

namespace Modules\Customer\Http\Controllers\Auth;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Customer\Entities\Customer;

class RegisterController extends CoreController
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function showRegisterForm()
    {
        return view('customer::auth.register');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function register(Request $request)
    {
        $this->validator($request);
        $user = $this->create($request->all());
        $this->guard('customer')->logout();
        $notification = array(
            'success' => 'Thank you for Registering with us.',
        );
        return redirect(route('customers.login'))->with($notification);
    }

    protected function validator(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email'    => 'required|email|min:5|max:191|unique:customers,email',
            'password' => 'required|string|confirmed|min:4|max:255',
        ];
        $request->validate($rules);
    }

    protected function create(array $data)
    {
        $user = Customer::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        return $user;
    }
}
