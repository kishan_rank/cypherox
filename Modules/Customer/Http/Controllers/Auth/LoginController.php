<?php

namespace Modules\Customer\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public $maxAttempts = 5;

    public $decayMinutes = 3;

    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }

    public function showLoginForm()
    {
        return view('customer::auth.login');
    }

    public function login(Request $request)
    {
        $this->validator($request);

        //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)) {
            //Fire the lockout event
            $this->fireLockoutEvent($request);

            //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('customer')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            return $this->authenticated($request, Auth::guard('customer')->user());
        }

        $this->incrementLoginAttempts($request);

        return $this->loginFailed();
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect()->route('customers.index');
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect()
            ->route('customers.login')
            ->with('success', 'Customer has been logged out!');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'email'    => 'required|email|exists:customers|min:5|max:191',
            'password' => 'required|string|min:4|max:255',

        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        $notification = array(
            'error' => 'Password Mismatch, please check credentials and try again!',
        );
        return redirect()
            ->back()
            ->withInput()
            ->with($notification);
    }

    public function username()
    {
        return 'email';
    }
}
