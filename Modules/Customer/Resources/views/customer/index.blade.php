@extends('theme::layouts.customer.master')

@section('title')
    {{ 'Book Vehicle' }}
@stop

@section('content')
    <div class="section">
        <div class="imgs-wrapper">
        </div>
        <div class="container col-md-12">
            <form method="post" class="form-horizontal" action="{{ route('customers.bookvehicle') }}">
                @csrf
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Select Vehicle:</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="vehicle_id">
                                <option>-- Select --</option>
                                @foreach ($vehicles as $vehicle)
                                    <option value="{{ $vehicle->id }}">{{ $vehicle->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Select Date:</label>
                        <div class="col-sm-9">
                            <input type="text" name="start_date" id="start_date" class="form-control" placeholder="date">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Booking For:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="book_for" name="book_for">
                                <option value="full">Full Day</option>
                                <option value="half">Half Day</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="bk_session" style="display:none">
                        <label class="control-label col-sm-3">Booking session:</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="shift">
                                <option value="morning">Morning</option>
                                <option value="evening">Evening</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Name:</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" placeholder="Enter Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Email:</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" class="form-control" placeholder="Enter Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Phone:</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone_number" class="form-control" placeholder="Enter Phone Number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Date Of Birth:</label>
                        <div class="col-sm-9">
                            <input type="text" name="birth_date" id="birth_date" class="form-control"
                                placeholder="DD\MM\YYYY">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Address:</label>
                        <div class="col-sm-9">
                            <input type="text" name="address" class="form-control" placeholder="Address">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-3">ZIP:</label>
                        <div class="col-sm-9">
                            <input type="text" name="zipcode" class="form-control" placeholder="ZIP Code">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">City:</label>
                        <div class="col-sm-9">
                            <input type="text" name="city" class="form-control" placeholder="City">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">State:</label>
                        <div class="col-sm-9">
                            <input type="text" name="state" class="form-control" placeholder="State">
                        </div>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
@push('before-js')
@endpush

@push('script')
    <script type="text/javascript">
        $("#book_for").change(function() {
            if ($(this).val() == "half") {
                $("#bk_session").show();
            } else {
                $("#bk_session").hide();
            }
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery("#start_date").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
            });
            jQuery("#birth_date").datepicker({
                dateFormat: 'yy-mm-dd',
            });
        });
    </script>
@endpush
