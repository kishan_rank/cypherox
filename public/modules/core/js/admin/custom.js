// Default Ajax Loader
var showIndicator = true;
var beenSubmitted = false;
if (!beenSubmitted) {
    loaderHide();
}

function loaderShow() {
    $('.custom-loader').show();
}
function loaderHide() {
    $('.custom-loader').hide();
}
var showIndicator = true;

function ajaxindicatorstart(text) {
    if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
        var html = `<div id="resultLoading"><div class="spinner"><div class="dot1"></div><div class="dot2"></div></div><div class="modal-backdrop fade in"></div></div>`;
        jQuery('body').append(html);
    }
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop() {
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

jQuery(document).ajaxStart(function () {
    if (showIndicator) {
        //show ajax indicator
        ajaxindicatorstart('please wait..');
    }
}).ajaxStop(function () {
    //hide ajax indicator
    ajaxindicatorstop();
});

jQuery(document).ready(() => {

    jQuery(".nav-treeview").each(function () {
        if (jQuery(this).children().length == 0) {
            jQuery(this).parent().remove();
        }
    });


    jQuery(".save").on("click", function () {
        var formId = jQuery(this).attr("data-form-id");
        jQuery(`#${formId} .snc`).val(0);
        jQuery(`#${formId}`).submit();
    });

    jQuery(".savencontinue").on("click", function () {
        var formId = jQuery(this).attr("data-form-id");
        jQuery(`#${formId} .snc`).val(1);
        jQuery(`#${formId}`).submit();
    });

    // When the user scrolls the page, execute myFunction
    window.onscroll = function () {
        // fixScroll()
    };
    // Add the sticky class to the fixElement when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function fixScroll() {
        // Get the header
        var fixElement = document.getElementsByClassName("content-header");
        if (fixElement) {
            // Get the offset position of the navbar
            var sticky = fixElement[0].offsetTop;
            if (window.pageYOffset > sticky) {
                fixElement[0].classList.add("sticky-header");
            } else {
                fixElement[0].classList.remove("sticky-header");
            }
        }
    }
})
